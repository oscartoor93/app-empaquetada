const { app, BrowserWindow } = require('electron')

function createWindow () {
    // Crea la ventana del navegador.
    let win = new BrowserWindow({ width: 800, height: 600 })

    // y cargue el index.html de su aplicación.
    win.loadURL(`file://${__dirname}/index.html`);}

app.on('ready', createWindow)